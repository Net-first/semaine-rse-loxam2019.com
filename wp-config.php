<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'suitcaseoqprepro');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'suitcaseoqprepro');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'PNXTN4sJCk');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'suitcaseoqprepro.mysql.db');

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'SECURE_AUTH_KEY',  'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'LOGGED_IN_KEY',    'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'NONCE_KEY',        'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'AUTH_SALT',        'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'SECURE_AUTH_SALT', 'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'LOGGED_IN_SALT',   'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );
define( 'NONCE_SALT',       'UMvbM3&N!>2T:6F/G~S*$uR&53J^vBEF-:3Bwsex' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'lox_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}


/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');


//Rename wp-content folder
define ('WP_CONTENT_FOLDERNAME', 'loxfiles');

//Define new directory path
define ('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
 
//Define new directory URL
define('WP_SITEURL', 'https://' . $_SERVER['HTTP_HOST'] . '/');
define('WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME);


/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
