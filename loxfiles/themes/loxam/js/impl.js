jQuery(function() {
    resetPassword();
    testHandler();
    //timerAnimation("00:00");
});

function timerAnimation(t){// other ways --> "0:15" "03:5" "5:2"
    //TweenLite.defaultEase = Expo.easeOut;
    var timerEl = document.querySelector('.counter-time'),
    minutesGroupEl = timerEl.querySelector('.minutes-group'),
    secondsGroupEl = timerEl.querySelector('.seconds-group'),

    minutesGroup = {
	firstNum: minutesGroupEl.querySelector('.tens'),
	secondNum: minutesGroupEl.querySelector('.ones')
    },

    secondsGroup = {
	firstNum: secondsGroupEl.querySelector('.tens'),
	secondNum: secondsGroupEl.querySelector('.ones')
    };

    var time = {
	min: t.split(':')[0],
	sec: t.split(':')[1]
    };

    function updateTimer() {
 
	var timestr;
	var date = new Date();
  
	date.setHours(0);
	date.setMinutes(time.min);
	date.setSeconds(time.sec);

	var newDate = new Date(date.valueOf() + 1000);
	var temp = newDate.toTimeString().split(" ");
	var tempsplit = temp[0].split(':');
 
	time.min = tempsplit[1];
	time.sec = tempsplit[2];

	timestr = time.min + time.sec;

	updateTimerDisplay(timestr.split(''));

	setTimeout(updateTimer, 1000);
    }

    function updateTimerDisplay(arr) {
	animateNum(minutesGroup.firstNum, arr[0]);
	animateNum(minutesGroup.secondNum, arr[1]);
	animateNum(secondsGroup.firstNum, arr[2]);
	animateNum(secondsGroup.secondNum, arr[3]);
    }

    function animateNum (group, arrayValue) {
	TweenMax.killTweensOf(group.querySelector('.digit-wrapper'));
	TweenMax.to(group.querySelector('.digit-wrapper'), 1, {
	    y: - group.querySelector('.num-' + arrayValue).offsetTop
	});
    }
   
    updateTimer();
}

function updateUserData(){
    var scoreHolder = jQuery( '#u_score' );
    var timeHolder = jQuery( '#u_time' );
    var userScore = Cookies.get( 'u_score' );
    var userTime = Cookies.get( 'u_time' );
    
    if (typeof userScore !== 'undefined') {
        scoreHolder.text( userScore );
    }
    
    if (typeof userTime !== 'undefined') {
        timeHolder.text( userTime );
    }
}

function testHandler(){
    var form = jQuery( '#ajax-test-holder' );
    
    function hideShowButton(){
        var button = form.find( 'button[type="submit"]' );
        form.find( 'input[name="u_answer[]"]' ).on( 'change', function(){
            if( validation() ){
                button.show();
            } else {
                button.hide();
            }   
        } );
    }
    
    function validation(){
        var inputs = form.find( 'input[name="u_answer[]"]' );
        var isValid = 0;

        if( inputs.length ) {
            inputs.each( function( i, input ) {
                if( jQuery( input ).prop( "checked" ) ){
                    isValid++;
                } 
            } );
        } else {
            return true;
        }
        
        return isValid;
    }
    
    function timerStart( response ) {
	var counterAnim = jQuery( 'ul.info-list .counter-time .count-up' );
	var counterDef = jQuery( 'ul.info-list .counter-time .count-stop' );
	var input = jQuery( response ).find('[name="quiz_action"]');
	var action = input.val();
	//console.log( action );
	if ( action == 'check' ) {
	    //animation start
	    counterDef.hide();
	    counterAnim.addClass( 'active' );
	    counterAnim.show();
	//} else if ( action == 'start' ) {
	} else {
	    //animation end
	    counterDef.show();
	    counterAnim.hide();
	    counterAnim.removeClass( 'active' );
	}
    }

    form.on( 'submit', function( e ){
        e.preventDefault();

        var statusElement = form.find( '.form-holder .container .lwa-status' );

        if( validation() ){
            if( statusElement.length ){
                statusElement.removeClass( 'lwa-status-invalid' );
            }
        } else {
            statusElement.addClass( 'lwa-status-invalid' );
            return;
        }
        
        var data = form.serialize();
        data += '&action=test_handler'; /// ???
        data += '&nonce_code=' + themeAjax.nonce;
 
        form.empty();
        
        var loader = jQuery('<div class="lwa-loading"></div>');
        loader.prependTo( form );
        
        jQuery.ajax({
            method: 'POST',
            url: themeAjax.url,
            data: data
        }).done(function( response ) {
	    timerStart( response );
	    
            form.append( response );
	    
            loader.remove();
            updateUserData();
            hideShowButton();
        });
    } );
}

function resetPassword() {
    var form = jQuery( '#change-pass' );
    form.on( 'submit', function( e ){
        e.preventDefault();

        var statusElement = form.find( '.lwa-status' );
        if( statusElement.length == 0 ){
            statusElement = jQuery( '<span class="lwa-status"></span>' );
            form.prepend( statusElement );
        }

        statusElement.removeClass( 'lwa-status-invalid' ).empty();
        
        var loader = jQuery('<div class="lwa-loading"></div>');
        loader.prependTo( form );

        var data = form.serialize();
        data += '&action=change_password';
        data += '&nonce_code=' + themeAjax.nonce;

        jQuery.ajax({
            method: 'POST',
            url: themeAjax.url,
            data: data
        }).done(function( response ) {
            var messageClass;
            if ( response.error) {
                messageClass = 'lwa-status-invalid';
            } else {
                messageClass = 'lwa-status-confirm';
                form.empty();
                form.prepend( statusElement );
            }
            
            statusElement.addClass( messageClass ).append( response.message );

            loader.remove();
        });
    } );
}

/*!
 * JavaScript Cookie v2.2.0
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function decode (s) {
		return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
	}

	function init (converter) {
		function api() {}

		function set (key, value, attributes) {
			if (typeof document === 'undefined') {
				return;
			}

			attributes = extend({
				path: '/'
			}, api.defaults, attributes);

			if (typeof attributes.expires === 'number') {
				attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5);
			}

			// We're using "expires" because "max-age" is not supported by IE
			attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

			try {
				var result = JSON.stringify(value);
				if (/^[\{\[]/.test(result)) {
					value = result;
				}
			} catch (e) {}

			value = converter.write ?
				converter.write(value, key) :
				encodeURIComponent(String(value))
					.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

			key = encodeURIComponent(String(key))
				.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)
				.replace(/[\(\)]/g, escape);

			var stringifiedAttributes = '';
			for (var attributeName in attributes) {
				if (!attributes[attributeName]) {
					continue;
				}
				stringifiedAttributes += '; ' + attributeName;
				if (attributes[attributeName] === true) {
					continue;
				}

				// Considers RFC 6265 section 5.2:
				// ...
				// 3.  If the remaining unparsed-attributes contains a %x3B (";")
				//     character:
				// Consume the characters of the unparsed-attributes up to,
				// not including, the first %x3B (";") character.
				// ...
				stringifiedAttributes += '=' + attributes[attributeName].split(';')[0];
			}

			return (document.cookie = key + '=' + value + stringifiedAttributes);
		}

		function get (key, json) {
			if (typeof document === 'undefined') {
				return;
			}

			var jar = {};
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all.
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (!json && cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = decode(parts[0]);
					cookie = (converter.read || converter)(cookie, name) ||
						decode(cookie);

					if (json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					jar[name] = cookie;

					if (key === name) {
						break;
					}
				} catch (e) {}
			}

			return key ? jar[key] : jar;
		}

		api.set = set;
		api.get = function (key) {
			return get(key, false /* read as raw */);
		};
		api.getJSON = function (key) {
			return get(key, true /* read as json */);
		};
		api.remove = function (key, attributes) {
			set(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.defaults = {};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));
