<?php get_header(); ?>
<div class="login-holder">
    <div class="container">
        <?php get_template_part( 'blocks/not_found' ); ?>
    </div>
</div>
<div class="main-img">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg-image-6.png" alt="image description">
</div>
<?php get_footer(); ?>