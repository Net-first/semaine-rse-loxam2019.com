<?php get_header(); the_post(); ?>
<div class="login-holder">
    <div class="container">
        <h1><?php the_title() ?></h1>
		<?php the_post_thumbnail( 'full' ); ?>
		<?php the_content(); ?>
		<?php edit_post_link( __( 'Edit', 'base' ) ); ?>
		<?php wp_link_pages(); ?>
		<?php comments_template(); ?>
    </div>
</div>
<div class="main-img">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg-image-6.png" alt="image description">
</div>
<?php get_footer(); ?>