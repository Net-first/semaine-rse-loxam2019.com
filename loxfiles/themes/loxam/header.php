<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="wrapper">
		<header id="header">
			<div class="container">
				<div class="logo">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>">
					</a>
				</div>
				<?php $first_row = get_field( 'option_top_slogan_first_row', 'option' );
				$second_row = get_field( 'option_top_slogan_second_row', 'option' );
				if( $first_row or $second_row ) : ?>
					<strong class="slogan"><?php if( $first_row ) : ?><span><?php echo $first_row ?></span> <?php endif ?><?php echo $second_row ?></strong>
				<?php endif ?>
				<?php if ( is_user_logged_in() ) :
					$user_id = get_current_user_id(); ?>
					<ul class="info-list">
						<li><?php echo theme_get_current_user_name(); ?></li>
						<li><?php _e( 'score', 'base' ) ?>&nbsp;<span id="u_score"><?php echo Quiz_Data::get_user_score( $user_id ) ?></span>/<?php echo Quiz_Data::get_max_score() ?></li>
						<li class="counter-time">
							<span class="count-stop">
								<?php _e( 'temps', 'base' ) ?> <span id="u_time"><?php echo theme_seconds_to_time( Quiz_Data::get_user_time( $user_id ) ) ?></span>
							</span>
							<?php /* //timerAnimation("00:00")
							<div class="count-up">
								<div class="time-part-wrapper minutes-group">
									<div class="time-part minutes tens">
										<div class="digit-wrapper">
											<span class="digit num-0">0</span>
											<span class="digit num-1">1</span>
											<span class="digit num-2">2</span>
											<span class="digit num-3">3</span>
											<span class="digit num-4">4</span>
											<span class="digit num-5">5</span>
										</div>
									</div>
									<div class="time-part minutes ones">
										<div class="digit-wrapper">
											<span class="digit num-0">0</span>
											<span class="digit num-1">1</span>
											<span class="digit num-2">2</span>
											<span class="digit num-3">3</span>
											<span class="digit num-4">4</span>
											<span class="digit num-5">5</span>
											<span class="digit num-6">6</span>
											<span class="digit num-7">7</span>
											<span class="digit num-8">8</span>
											<span class="digit num-9">9</span>
										</div>
									</div>
								</div>
								<div class="time-part-wrapper seconds-group">
									<div class="time-part seconds tens">
										<div class="digit-wrapper">
											<span class="digit num-0">0</span>
											<span class="digit num-1">1</span>
											<span class="digit num-2">2</span>
											<span class="digit num-3">3</span>
											<span class="digit num-4">4</span>
											<span class="digit num-5">5</span>
										</div>
									</div>
									<div class="time-part seconds ones">
										<div class="digit-wrapper">
											<span class="digit num-0">0</span>
											<span class="digit num-1">1</span>
											<span class="digit num-2">2</span>
											<span class="digit num-3">3</span>
											<span class="digit num-4">4</span>
											<span class="digit num-5">5</span>
											<span class="digit num-6">6</span>
											<span class="digit num-7">7</span>
											<span class="digit num-8">8</span>
											<span class="digit num-9">9</span>
										</div>
									</div>
								</div>
							</div>
							*/ ?>
							<div class="count-up" style="display: none;">
								<div class="time-part-wrapper minutes-group">
									<div class="time-part minutes tens">
										<div class="digit-wrapper">
											<span class="digit">0</span>
											<span class="digit">1</span>
											<span class="digit">2</span>
											<span class="digit">3</span>
											<span class="digit">4</span>
											<span class="digit">5</span>
										</div>
									</div>
									<div class="time-part minutes ones">
										<div class="digit-wrapper">
											<span class="digit">0</span>
											<span class="digit">1</span>
											<span class="digit">2</span>
											<span class="digit">3</span>
											<span class="digit">4</span>
											<span class="digit">5</span>
											<span class="digit">6</span>
											<span class="digit">7</span>
											<span class="digit">8</span>
											<span class="digit">9</span>
										</div>
									</div>
								</div>
								<div class="time-part-wrapper seconds-group">
									<div class="time-part seconds tens">
										<div class="digit-wrapper">
											<span class="digit">0</span>
											<span class="digit">1</span>
											<span class="digit">2</span>
											<span class="digit">3</span>
											<span class="digit">4</span>
											<span class="digit">5</span>
										</div>
									</div>
									<div class="time-part seconds ones">
										<div class="digit-wrapper">
											<span class="digit">0</span>
											<span class="digit">1</span>
											<span class="digit">2</span>
											<span class="digit">3</span>
											<span class="digit">4</span>
											<span class="digit">5</span>
											<span class="digit">6</span>
											<span class="digit">7</span>
											<span class="digit">8</span>
											<span class="digit">9</span>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				<?php else : ?>
					<?php do_action('wpml_add_language_selector'); ?>
				<?php endif ?>
			</div>
		</header>
		<main id="main">