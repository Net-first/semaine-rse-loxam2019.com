		</main>
		<?php if ( is_user_logged_in() and is_page_template( 'pages/template-quiz.php' ) ) :
			global $footer_rating; ?>
			<footer id="footer">
				<div class="container">
					<div class="line-box">
						<div class="mask">
							<div class="line">
								<?php if( $footer_rating ) :
									$u_result = Quiz_Data::get_top_users();
									$temp = array();
									$temp[] = __( 'BRAVO AUX MEILLEURES AGENCES !', 'base' );
									
									if ( ! empty( $u_result ) ) {
										$max_score = Quiz_Data::get_max_score();
										foreach ( $u_result as $user ) {
											$score = Quiz_Data::get_user_score( $user->ID );
											$time = theme_seconds_to_time( Quiz_Data::get_user_time( $user->ID ) );
											$name = theme_get_current_user_name( $user->ID );
											$temp[] = "{$name} {$score}/{$max_score} {$time}";
										}
									}
									?>
									<p><?php echo implode( ' - ', $temp ) ?></p>
								<?php else :
									$footer_text = get_field( 'option_footer_text', 'option' );
									
									echo wpautop( $footer_text );
								endif ?>
							</div>
						</div>
					</div>
				</div>
			</footer>
		<?php endif ?>
	</div>
	<?php wp_footer(); ?>
</body>
</html>