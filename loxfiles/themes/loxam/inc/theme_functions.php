<?php
//The Quiz will be active each day from 11am to 6pm
define( "TIME_OPEN", 90000 ); // 90000
define( "TIME_START", 110000 ); // 110000
define( "TIME_END", 180000 ); // 180000
define( "DATE_START", 20190812 ); // 20190527
define( "DATE_END", 20190816 ); // 20190531
define( "SUBSCRIBER_PASS", 'LOXAM2019ADFNNPA' );

add_filter( 'display_post_states', 'theme_add_post_state', 10, 2 );
function theme_add_post_state( $post_states, $_post ){
	$quiz_page_id = get_field( 'option_quiz_page', 'option' );
	$password_page_id = get_field( 'option_password_page', 'option' );
	$test_page_id = get_field( 'option_test_page', 'option' );
	if( $_post->ID == $quiz_page_id ){
		$post_states['theme_quiz_page'] = __( 'Quiz main page', 'base' );
	} else if( $_post->ID == $password_page_id ){
		$post_states['theme_password_page'] = __( 'Quiz password page', 'base' );
	} else if( $_post->ID == $test_page_id ){
		$post_states['theme_test_page'] = __( 'Quiz test page', 'base' );
	}

	return $post_states;
}

add_action( 'template_redirect', function(){
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		$activate_account = get_user_meta( $current_user->ID, '_activate_account', true );
		$option_password_page_id = get_field( 'option_password_page', 'option' );
		
		if( $option_password_page_id and ! $activate_account and ! is_page( $option_password_page_id ) ) {
			wp_redirect( get_permalink( $option_password_page_id ) );
			exit; 
		}

		$quiz_page_id = get_field( 'option_quiz_page', 'option' );
		if( $quiz_page_id and is_front_page() ) {
                        wp_redirect( get_permalink( $quiz_page_id ) );
                        exit;   
		}
	} elseif( ! is_user_logged_in() and ! is_front_page() ) {
		wp_redirect( home_url() );
		exit;
	}
}, 1 );

add_action( 'admin_init', 'theme_disable_dashboard' );
function theme_disable_dashboard() {
        global $pagenow;
        if ( theme_is_subscriber() and ( 'profile.php' == $pagenow or 'index.php' == $pagenow ) ) {
                wp_redirect( home_url() );
		exit;
        }
}

add_action( 'init', 'theme_remove_admin_bar' );
function theme_remove_admin_bar() {
	if ( theme_is_subscriber() ) {
                show_admin_bar( false );   
        }
}

add_filter( 'style_loader_tag', 'theme_style_loader_tag', 10, 4 );
function theme_style_loader_tag( $html, $handle, $href, $media ){
	if( 'base-fontawesome' == $handle ) {
		$html = str_replace( ' href=', ' integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous" href=', $html );
	}

	return $html;
}

add_filter( 'wp_pre_insert_user_data', 'theme_set_default_password', 10, 3 );
function theme_set_default_password( $data, $update, $id ){
	if( isset( $_POST[ 'role' ] ) and $_POST[ 'role' ] == 'subscriber' and ! $update and is_null( $id ) ){
		// set default password to all subscribers
		$default_password = SUBSCRIBER_PASS;
		$user_pass = wp_hash_password( $default_password );
		$data[ 'user_pass' ] = $user_pass;
	}

	return $data;
}

add_filter( 'insert_user_meta', 'theme_set_user_meta', 10, 3 );
function theme_set_user_meta( $meta, $user, $update ){
	if( isset( $_POST[ 'role' ] ) and $_POST[ 'role' ] == 'subscriber' and ! $update ){
		$meta[ '_activate_account' ] = 0;
	}

	return $meta;
}

add_action( 'wp_ajax_change_password', 'theme_ajax_change_password' );
function theme_ajax_change_password(){
	check_ajax_referer( 'theme-ajax-nonce', 'nonce_code' );
        $errors = new WP_Error;
        $current_user = wp_get_current_user();
        
        $response = array(
                        'message' => '',
                        'error' => 0,
                        );
        
        if( in_array( 'subscriber', $current_user->roles ) ) {
                $c = strip_tags( trim( $_POST[ 'c_pass' ] ) );
                $n = strip_tags( trim( $_POST[ 'n_pass' ] ) );
                $r = strip_tags( trim( $_POST[ 'r_pass' ] ) );
                
                if( empty( $c ) ){
                        $errors->add( 'empty_current_password', __( 'Empty current password', 'base' ) );
                } else {
                        if ( ! wp_check_password( $c, $current_user->data->user_pass ) ) {
                                $errors->add( 'wrong_current_password', __( 'Wrong current password', 'base' ) );
                        }
                }
                
                if( empty( $n ) ){
                        $errors->add( 'empty_new_password', __( 'Empty new password', 'base' ) );
                } 
                
                if( empty( $r ) ){
                        $errors->add( 'empty_repeat_password', __( 'Empty repeat password', 'base' ) );
                }
                
                if( $n != $r ) {
                        $errors->add( 'empty_repeat_password', __( 'Entered password does not match', 'base' ) );
                }
        } else {
               $errors->add( 'wrong_role', __( 'Reset password can only user with the role of subscriber', 'base' ) ); 
        }
     
        if ( $errors->get_error_code() ) {
                $temp = array();
                foreach( $errors->get_error_messages() as $i => $error ){
                        $temp[] = '<strong>' . __( 'Error', 'base' ) . '</strong>: ' . $error . '.';
                }
                $response[ 'message' ] = implode( '<br>', $temp );
                $response[ 'error' ] = 1;
        } else {
                update_user_meta( $current_user->ID, '_activate_account', 1, 0 );
                add_user_meta( $current_user->ID, '_user_ip', geoip_detect2_get_client_ip(), true );
                wp_set_password( $n, $current_user->ID );
                
                $temp = __( 'You have successfully changed your password. ', 'base' );
                $temp .= '<br>';
                $temp .= '<a href="' . home_url() . '">' . __( 'Please login again.', 'base' ) . '</a>';
                $response[ 'message' ] = $temp;
        }
        
        header('Content-Type: application/json');
        echo json_encode( $response );
      
	wp_die();
}

function theme_get_user_time(){
	$result = array();
	
	$ip = geoip_detect2_get_client_ip();
	$record = geoip_detect2_get_info_from_ip( $ip, NULL );
	$tz = $record->location->timeZone;
	//$tz = 'Europe/London';
	//$tz = 'America/Adak';
	$timestamp = time();
	$dt = new DateTime( "now", new DateTimeZone( $tz ) );
	$dt->setTimestamp( $timestamp );
	$result[ 'user_date' ] = $dt->format( 'Ymd' ); // 20190513
	$result[ 'user_time' ] = $dt->format( 'Gis' ); // 130757 10757
	
	return $result;
}

function theme_seconds_to_time( $seconds ) {
        $dt = new DateTime( '@' . $seconds, new DateTimeZone( 'UTC' ) );
        if( $seconds >= 3600 ) {
                $time = $dt->format('H:i:s');
        } else {
                $time = $dt->format('i:s');
        }
        
        return $time;
}

/*
// do not remove, use only if you know what it is
function theme_reset_quiz_data(){
	$res = Quiz_Data::get_user_ids();
	if( $res ) {
		foreach( $res as $obj ) {
			Quiz_Data::reset_user_data( $obj->user_id );
		}
	}
}
*/

class Quiz_Data{
        private static $score_key = 'score' . '-' . DATE_START . '-' . DATE_END;
        private static $time_key = 'time' . '-' . DATE_START . '-' . DATE_END;
        
        public static function get_time_key() {
                return self::$time_key;     
        }
        
        public static function get_score_key() {
                return self::$score_key;     
        }
        
        public static function get_questions_data( $user_id, $question_date ) {
                global $wpdb;
                $table_name = $wpdb->prefix . 'q_results';
                
                $results = $wpdb->get_results( $wpdb->prepare( 
                        "
                                SELECT * 
                                FROM $table_name 
                                WHERE user_id = %d
                                AND question_date = %d
								ORDER BY question_id
								DESC
                        ", 
                        $user_id, $question_date
                ) );
         
                return $results;
        }
        
        public static function get_row( $row_id ) {
                global $wpdb;
                $table_name = $wpdb->prefix . 'q_results';
                
                $row = $wpdb->get_row( $wpdb->prepare( 
                        "
                                SELECT * 
                                FROM $table_name 
                                WHERE id = %d
                        ", 
                        $row_id
                ) );
           
                return $row;
        }
	
	public static function get_user_ids(){
		global $wpdb;
                $table_name = $wpdb->prefix . 'q_results';
                
                $ids = $wpdb->get_results( 
                        "
                                SELECT user_id 
                                FROM $table_name 
                                WHERE 1 = 1
				GROUP BY user_id
                        "
                );
           
                return $ids;
	}
        
        public static function question_data_start( $user_id, $quiz_id, $question_id, $question_date, $question ) {
                global $wpdb;
                $table_name = $wpdb->prefix . 'q_results';
          
                $wpdb->insert( 
                        $table_name, 
                        array( 
                                'user_id' => $user_id, 
                                'quiz_id' => $quiz_id,
                                'question_id' => $question_id,
                                'question_date' => $question_date,
                                'question' => $question,
                                'user_answer' => '',
                                'correct_answer' => '',
                                'time_start' => time(),
                                'time_end' => 0,
                                'time_diff' => 0,
                                'score' => 0,
                                'language' => ICL_LANGUAGE_CODE,
                                'is_subscriber' => theme_is_subscriber() ? 'yes' : 'no',
                        ), 
                        array( 
                                '%d', 
                                '%d',
                                '%d',
                                '%d',
                                '%s',
                                '%s',
                                '%s',
                                '%d',
                                '%d',
                                '%d',
                                '%d',
                                '%s',
                                '%s',
                        ) 
                );
				
				/*
				$wpdb->show_errors();
				$wpdb->print_error();
                */
				
                return $wpdb->insert_id;
        }
        
        public static function question_data_end( $row_id, $user_answer, $correct_answer, $time_end, $time_diff, $score ) {
                global $wpdb;
                $table_name = $wpdb->prefix . 'q_results';
                
                $wpdb->update( 
                        $table_name, 
                        array( 
                                'user_answer' => $user_answer,
                                'correct_answer' => $correct_answer,
                                'time_end' => $time_end,
                                'time_diff' => $time_diff,
                                'score' => $score,
                        ), 
                        array( 'id' => $row_id ), 
                        array( 
                                '%s',
                                '%s',
                                '%d',
                                '%d',
                                '%d',
                        ), 
                        array( '%d' ) 
                );
			/*
				$wpdb->show_errors();
				$wpdb->print_error();
               */
        }
        
        public static function get_top_users( $limit = 20 ) {
			$args = array(
					'role' => 'Subscriber',
					'number' => $limit,
					'meta_query' => array(
									array(
										'relation' => 'AND',
										'score' => array(
											'key' => Quiz_Data::get_score_key(),
											'type' => 'numeric',
											'compare' => 'EXISTS',
										),
										'time' => array(
											'key' => Quiz_Data::get_time_key(),
											'type' => 'numeric',
											'compare' => 'EXISTS',
										),
									)
							  ),
					'orderby' => array(
					'score'  => 'desc',
					'time' => 'asc',
					),
				);
			$user_query = new WP_User_Query( $args );
			
			return $user_query->get_results();
        }
        
        public static function update_user_score( $user_id, $num ) {
				//if( theme_is_subscriber() ) {
                        $_score = self::get_user_score( $user_id );
						setcookie( 'u_score', $_score + $num, 0, '/' );
                        update_user_meta( $user_id, self::$score_key, $_score + $num, $_score );  
                //}
        }
        
        public static function get_user_score( $user_id ) {
                return absint( get_user_meta( $user_id, self::$score_key, true ) );
        }
        
        public static function update_user_time( $user_id, $sec ) {
                //if( theme_is_subscriber() ) {
                        $_seconds = self::get_user_time( $user_id );
						setcookie( 'u_time', theme_seconds_to_time( $_seconds + $sec ), 0, '/' );
                        update_user_meta( $user_id, self::$time_key, $_seconds + $sec, $_seconds );  
                //}
        }
        
        public static function get_user_time( $user_id ) {
                return absint( get_user_meta( $user_id, self::$time_key, true ) );
        }
        
        public static function reset_user_data( $user_id ) {
                delete_user_meta( $user_id, self::$score_key );
                delete_user_meta( $user_id, self::$time_key );
                
                global $wpdb;
                $table_name = $wpdb->prefix . 'q_results';
                
                $wpdb->query( 
                        $wpdb->prepare( 
                                "
                                DELETE FROM $table_name
                                WHERE user_id = %d
                                ",
                                $user_id
                        )
                );
        }
		
		public static function get_max_score(){
			$score_max_key = self::$score_key . '-max';

			$g_score_max = get_transient( $score_max_key );
			
			if ( false !== $g_score_max ) {
				return $g_score_max;
			}

			$query = new WP_Query( array(
									'post_type' => 'quiz',
									'posts_per_page' => -1,
									'post_status' => 'publish',
									'ignore_sticky_posts' => true,
									'order' => 'ASC',
									'orderby' => 'meta_value_num',
									'meta_query' => array(
														array(
																'key'     => 'quiz_date',
																'value'   => array( DATE_START, DATE_END ),
																'compare' => 'BETWEEN',
														),
													),
									) );
			$g_score_max = 0;

			if( $query->have_posts() ) :
				while ( $query->have_posts() ) : $query->the_post();
					$quiz_questions = get_field( 'quiz_questions', get_the_ID() );
					$quiz_score = get_field( 'quiz_questions_score', get_the_ID() );
					$g_score_max += $quiz_score * count( $quiz_questions );
				endwhile;
			endif;
			wp_reset_postdata();
			
			set_transient( $score_max_key, $g_score_max, WEEK_IN_SECONDS );
			
			return $g_score_max;
		}
}

function theme_quiz_has_result( $user_id, $date ){
	$upload_dir = wp_upload_dir();
	$target = $upload_dir['basedir'] . '/quiz-html-data/';
	$html_file = $target . $user_id . '-' . $date . '.html';
	
	return file_exists( $html_file );
}

function theme_quiz_results_html( $user_id, $date ){
	// completed test
	$upload_dir = wp_upload_dir();
	$target = $upload_dir['basedir'] . '/quiz-html-data/';
	$html_file = $target . $user_id . '-' . $date . '.html';

	if( file_exists( $html_file ) ) {
		include( $html_file );
	} else {
		if( ! file_exists( $target ) ){
			@mkdir( $target  );
		}

		ob_start();
		
		$file = get_template_directory() . '/blocks/ajax-quiz/results.php';
		include( $file );
		
		$html = ob_get_contents();
		ob_end_clean();
		
		file_put_contents( $html_file, $html );
		echo $html;
	}
}

function theme_quiz_completed_results_html( $_quiz_id, $user_id, $date ){
	// completed test
	$upload_dir = wp_upload_dir();
	$target = $upload_dir['basedir'] . '/quiz-html-data/';
	$html_file = $target . $user_id . '-' . $date . '-slider.html';

	if( file_exists( $html_file ) ) {
		include( $html_file );
	} else {
		if( ! file_exists( $target ) ){
			@mkdir( $target  );
		}

		ob_start();
		
		$file = get_template_directory() . '/blocks/ajax-quiz/results-slider.php';
		include( $file );
		
		$html = ob_get_contents();
		ob_end_clean();
		
		file_put_contents( $html_file, $html );
		echo $html;
	}
}

function theme_ajax_html_error( $text ){
	?>
	<div class="container">
		<span class="lwa-status lwa-status-invalid" style="text-align: center;"><?php echo $text ?></span>
	</div>
	<?php
}

function theme_is_subscriber(){
	$current_user = wp_get_current_user();
	
	return in_array( 'subscriber', $current_user->roles );
}

function theme_get_current_user_name( $user_id = 0 ){
	$name = '';
	if( $user_id ){
		$user_agency = get_user_meta( $user_id, 'user_agency', true );
		if( $user_agency ) {
			$name = __( 'Agence', 'base' ) . ' ' . $user_agency;
		} else {
			$user = get_user_by( 'id', $user_id );
			$name = $user->display_name;
		}
	} else {
		$current_user = wp_get_current_user();
		$name = $current_user->display_name;
	}
	
	return $name;
}

add_action( 'wp_ajax_test_handler', 'theme_ajax_test_handler' );
function theme_ajax_test_handler(){
	check_ajax_referer( 'theme-ajax-nonce', 'nonce_code' );
	$user_id = get_current_user_id();

	$_active = '';
	$_complete = '';
	$_quiz_id = isset( $_POST['quiz_id'] ) ? absint( $_POST['quiz_id'] ) : 0;
	if( $_quiz_id ) {
		$date = absint( get_post_meta( $_quiz_id, 'quiz_date', true ) );
		
		$u_date = theme_get_user_time();
		$_user_date = $u_date[ 'user_date' ]; // 20190513
		$_user_time = $u_date[ 'user_time' ]; // 130757 10757
		
		if( $date ) {
			if( $_user_date == $date and $_user_time >= TIME_START and $_user_time <= TIME_END ){
				$_active = 1;
			} else if( $_user_date > $date or ( $_user_date == $date and $_user_time > TIME_END ) ) {
				$_complete = 1;
			}
		}
	}
	
	if( $_active ) :
		$q_data = Quiz_Data::get_questions_data( $user_id, $date );
		$quiz_questions = get_field( 'quiz_questions', $_quiz_id );
		$quiz_question = array();
		$current_question = 0;
		$count_questions = count( $quiz_questions );
		$title = get_field( 'quiz_box_title', $_quiz_id );

		if( isset( $_POST[ 'quiz_action' ] ) and $_POST[ 'quiz_action' ] == 'start' ) :
			$__continue = false;
			$next_id = 0;
			$row_id = 0;
			if( ! empty( $q_data ) ) {
				foreach( $q_data as $q_row ){
					if( $q_row->time_end == 0 ){
						$current_question = $q_row->question_id;
						$quiz_question = $quiz_questions[ $current_question - 1 ];
						$row_id = $q_row->id;
						$__continue = true;
					}
					
					if( $q_row->question_id > $next_id ){
						$next_id = $q_row->question_id;
					}
				}
			}
			
			if( empty( $quiz_question ) and isset( $quiz_questions[ $next_id ] ) ){
				$quiz_question = $quiz_questions[ $next_id ];
				$current_question = $next_id + 1;
			}

			if( $quiz_question ) { 
				$layout = $quiz_question['acf_fc_layout'];
				$question = $quiz_question['question'];
				$answers = $quiz_question['answers'];
				
				if( ! $__continue ) {
					Quiz_Data::update_user_score( $user_id, 0 );
					Quiz_Data::update_user_time( $user_id, 0 );
                                        
					$row_id = Quiz_Data::question_data_start( $user_id, $_quiz_id, $current_question, $date, $question );
				}
				
				$hash = wp_hash( $_quiz_id . '-' . $row_id );

				$file = get_template_directory() . '/blocks/ajax-quiz/question_' . $layout . '.php';
				
				if( $row_id == 0 ) {
					theme_ajax_html_error( __( 'DB ERROR, please contact with admin', 'base' ) );
				} elseif( file_exists( $file ) ) {
					include( $file );
				}
			} else {
				// completed test
				theme_quiz_results_html( $user_id, $date );
			}
		elseif( isset( $_POST[ 'quiz_action' ] ) and $_POST[ 'quiz_action' ] == 'check' ) :
			$row_id = absint( $_POST['quiz_current_question_id'] );
			$hash = wp_hash( $_quiz_id . '-' . $row_id );
			
			if( isset( $_POST['quiz_hash'] ) and $_POST['quiz_hash'] == $hash ) {
				$temp_quiz_score = get_field( 'quiz_questions_score', $_quiz_id );
				$quiz_score = $temp_quiz_score ? $temp_quiz_score : 1;
                                
				$u_answer = $_POST['u_answer'];
				$row_data = Quiz_Data::get_row( $row_id );
				$current_question = $row_data->question_id;
				$question_id = $current_question - 1;
				$answers = $quiz_questions[ $question_id ]['answers'];
				$layout = $quiz_questions[ $question_id ]['acf_fc_layout'];
				$time_start = $row_data->time_start;
				$time_end = time();
				$time_diff = $time_end - $time_start;
				$score = 0;
				$correct_answer = '';
				$user_answer = '';

				$correct_answers = array();
				//$user_answers = array(); // = $u_answer

				$letter = 'A';
				foreach( $answers as $answer ) :
					if( $layout == 'images' ){
						$_answer = '<img src="' . $answer['answer']['url'] . '">';
					} elseif ( $layout == 'images_text' ) {
						$_answer = '<strong>' . $answer['answer_title'] . '</strong><br>
									<img src="' . $answer['answer']['url'] . '" alt="' . $answer['answer']['alt'] . '"><br>
									<strong>' . $answer['answer_subtitle'] . '</strong>
									<span>' . $answer['answer_text'] . '</span>';
					} else {
						$_answer = $answer['answer'];
					}
	
					if( $answer['correct_answer'] ) {
						$correct_answers[] = $letter;
						$correct_answer .=  wpautop( "{$letter} : {$_answer}" );
					}
					
					if( in_array( $letter, $u_answer ) ){
						$user_answer .= wpautop( "{$letter} : {$_answer}" );
					}
			
					$letter++;
				endforeach;
			
				$result = array_diff( $u_answer, $correct_answers );
				if( count( $u_answer ) == count( $correct_answers ) and empty( $result ) ) {
					$score = $quiz_score;
                    $result_answer_data = $quiz_questions[ $question_id ]['if_the_correct_answer'];
				} else {
					$result_answer_data = $quiz_questions[ $question_id ]['if_the_answer_is_incorrect'];
				}

				Quiz_Data::update_user_score( $user_id, $score );
				Quiz_Data::update_user_time( $user_id, $time_diff );
			  
				Quiz_Data::question_data_end( $row_id, $user_answer, $correct_answer, $time_end, $time_diff, $score );
			
				$file = get_template_directory() . '/blocks/ajax-quiz/answer_result.php';
				include( $file );
			} else {
				theme_ajax_html_error( __( 'Wrong question key!', 'base' ) ); // wrong hash
			} 
		else :
				theme_ajax_html_error( __( 'Wrong action, please reload page!', 'base' ) );
		endif;
	elseif( $_complete ) :
		theme_ajax_html_error( __( 'Test completed', 'base' ) );
	else :
		theme_ajax_html_error( __( 'Access denied!', 'base' ) );
	endif;

	wp_die();
}