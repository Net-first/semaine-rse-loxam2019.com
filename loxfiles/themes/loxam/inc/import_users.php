<?php trait Singleton {
	static private $instance = null;
    
	private function __construct() { /* ... @return Singleton */ } 
	private function __clone() { /* ... @return Singleton */ }
	private function __wakeup() { /* ... @return Singleton */ }
    
	static public function getInstance() {
		return 
		self::$instance===null
			? self::$instance = new static()//new self()
			: self::$instance;
	}
}

class Import_Users {
	use Singleton;

	public function create_page() {
		add_action( 'admin_menu',function(){
					add_users_page(
						__( 'Import Users' , 'base'),
						__( 'Import' , 'base'),
						'create_users',
						'import-users',
						array( __CLASS__ , 'html' )
						);
				} );			
		add_action( 'init', array( __CLASS__ , 'save_users' ) );
		add_action( 'admin_init', array( __CLASS__, 'admin_head' ) );
	}
	
	public static function save_users() {
		if ( isset( $_POST[ 'import-users' ] ) and isset( $_GET[ 'page' ] ) and $_GET['page'] == 'import-users' and check_admin_referer( 'import-users-from-file-xlsx', 'check-import' ) ) {
			if ( isset( $_FILES['users_xlsx']['tmp_name'] ) ) {
				$file = $_FILES['users_xlsx']['tmp_name'];
				$upload_dir = wp_upload_dir();
				$dir = $upload_dir['basedir'] . '/';
				$main_folder = $dir . 'temp_xlsx/';
				$temp_dir = $main_folder . 'temp';
				
				$log_data = array();
				
				if( ! file_exists( $file ) ) {
					wp_redirect( add_query_arg( 'error', '1', wp_get_referer() ) ); // file not selected
					exit;
				}	
				
				$extension = pathinfo( $_FILES['users_xlsx']['name'], PATHINFO_EXTENSION );
				if( $extension != 'xlsx' ) {
					wp_redirect( add_query_arg( 'error', '2', wp_get_referer() ) ); // wrong type of file
					exit;
				}
				
				if( ! is_dir( $temp_dir ) ) {
					if ( ! mkdir( $temp_dir, 0777, true ) ) {
						wp_redirect( add_query_arg( 'error', '3', wp_get_referer() ) ); // failed to create temp folder
						exit;
					}
				}
				
				$zip = new ZipArchive();
				$zip->open( $file );
				$zip->extractTo( $temp_dir );
				
				$strings = simplexml_load_file( $temp_dir . '/xl/sharedStrings.xml' );
				$sheet = simplexml_load_file( $temp_dir . '/xl/worksheets/sheet1.xml' );
			    
				$xlrows = $sheet->sheetData->row;
				$headers = array();
				
				foreach ( $xlrows as $xlrow ) {
					$arr = array();
					
					// In each row, grab it's value
					foreach ( $xlrow->c as $cell ) {
						$v = (string) $cell->v;
						
						// If it has a "t" (type?) of "s" (string?), use the value to look up string value
						if ( isset( $cell['t'] ) && $cell['t'] == 's' ) {
							$s  = array();
							$si = $strings->si[(int) $v];
							
							// Register & alias the default namespace or you'll get empty results in the xpath query
							$si->registerXPathNamespace( 'n', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main' );
							// Cat together all of the 't' (text?) node values
							foreach( $si->xpath('.//n:t') as $t ) {
							    $s[] = (string) $t;
							}
							$v = implode( $s );
						}
						
						$arr[] = $v;
					}
					
					// Assuming the first row are headers, stick them in the headers array
					if ( count( $headers ) == 0 ) {
						$headers = $arr;
					} else {
						// Combine the row with the headers - make sure we have the same column count
						$values = array_pad( $arr, count( $headers ), '' );
						$row = array_combine( $headers, $values );
						
						// processing data
						$first_name = isset( $values[0] ) ? ucfirst( strtolower( trim( $values[0] ) ) ) : '';
						$last_name = isset( $values[1] ) ? ucfirst( strtolower( trim( $values[1] ) ) ) : '';
						$company = isset( $values[2] ) ? strtolower( trim( $values[2] ) ) : '';
						$agency = isset( $values[3] ) ? trim( $values[3] ) : '';
						$email = isset( $values[4] ) ? trim( strtolower( $values[4] ) ) : '';
						$language  = isset( $values[5] ) ? trim( $values[5] ) : '';
						$crafts  = isset( $values[6] ) ? trim( $values[6] ) : '';
						
						$login = $email;

						$errors = [];

						if( empty( $email ) ) {
							$errors[] = __( 'Empty email', 'base' );
						} elseif ( ! is_email( $email ) ) {
							$errors[] = __( 'Email address is invalid', 'base' );
						} elseif ( email_exists( $email ) ) {
							$errors[] = __( 'Email already exists', 'base' );
						}

						if( empty( $errors ) ) {
							// insert user
							$userdata = array(
									'user_pass' => SUBSCRIBER_PASS,
									'user_login' => $login,
									'first_name' => $first_name,
									'last_name' => $last_name,
									'user_email' => $email,
									'role' => 'subscriber',
								);
							$user_id = wp_insert_user( $userdata );
		
							if ( is_wp_error( $user_id ) ) {
								$values[ 'result' ] = $user_id->get_error_message();
								$log_data[ 'failing' ][] = $values;
							} else {
								update_user_meta( $user_id, 'user_company_exp', $company );
								update_user_meta( $user_id, 'user_agency', $agency );
								update_user_meta( $user_id, 'user_language', $language );
								update_user_meta( $user_id, 'user_crafts', $crafts );
								
								$values[ 'result' ] = '';
								$log_data[ 'success' ][] = $values;
							}
						} else {
							if( empty( $first_name ) and empty( $last_name ) and empty( $email ) and empty( $login ) ){
								// remove empty row
							} else{
								$values[ 'result' ] = implode( '<br>', $errors );
								$log_data[ 'failing' ][] = $values;
							}
						}
					}
				}
				
				self::remove_folder_with_files( $temp_dir );
				
				$log_post = array(
					'post_type' => 'import-users-logs',
					'post_title' => date( 'Y-m-d H:i:s' ),
					'post_content'  => json_encode( $log_data ),
				      );
 
				// Insert the post into the database
				$_id = wp_insert_post( $log_post );
				if( ! is_wp_error( $_id ) ) {
					wp_redirect( add_query_arg( 'logs', $_id, wp_get_referer() ) ); // ok
					exit;
				}
				
				wp_redirect( add_query_arg( 'error', '4', wp_get_referer() ) ); // ok
				exit;
			}
		}
	}
	
	public static function html() {
		if ( isset( $_GET[ 'page' ] ) and $_GET['page'] == 'import-users' and isset( $_GET[ 'error' ] ) and ! isset( $_GET[ 'logs' ] ) ) {
			switch ( $_GET['error'] ) {
				case 1:
					echo '<div class="error notice"><p>' . __( 'No file selected' , 'base') . '</p></div>';
					break;
				case 2:
					echo '<div class="error notice"><p>' . __( 'Invalid file type' , 'base') . '</p></div>';
					break;
				case 3:
					echo '<div class="error notice"><p>' . __( 'Could not create temporary folder', 'base' ) . '</p></div>';
					break;
				case 4:
					echo '<div class="error notice"><p>' . __( 'No log information was created' , 'base') . '</p></div>';
					break;
				default:
					break;
			}
		} ?>
		<div class="wrap">
			<h1><?php _e( 'Import users' , 'base'); ?></h1>
			<?php 
			if ( isset( $_GET[ 'page' ] ) and $_GET['page'] == 'import-users' and isset( $_GET[ 'logs' ] ) ) : ?>
				<h2><a href="<?php echo admin_url( 'users.php?page=import-users&logs=all' ) ?>"><?php _e( 'Logs', 'base' ) ?></a></h2>
				<?php $log_id = absint( $_GET[ 'logs' ] );
				if( $log_id ) : ?>
					<div id="result">
						<?php
						$log_post = get_post( $log_id );
						if( $log_post ) {
							$json = $log_post->post_content;
							$data = json_decode( $json, true );
							if( isset( $data[ 'success' ] ) ) {
								$title = count( $data[ 'success' ] ) . __( ' user(s) added' , 'base' );
								self::show_results( $data[ 'success' ], 'green', $title );
							}
							if( isset( $data[ 'failing' ] ) ) {
								$title = __( 'Errors', 'base' );
								self::show_results( $data[ 'failing' ], 'red', $title );
							}
						} else {
							echo '<h2 style="color:red;">' . __( 'Log information does not exist', 'base' ) . '</h2>';	
						} ?>
					</div>
				<?php else :
					$query = new WP_Query( array(
						'post_type' => 'import-users-logs',
						'posts_per_page' => -1,
						'post_status' => 'draft',
							) );
					if($query->have_posts()) : ?>
						<table class="widefat striped">
							<?php while ( $query->have_posts() ) : $query->the_post(); ?>
								<tr>
									<td>
										<a href="<?php echo admin_url( 'users.php?page=import-users&logs=' . get_the_ID() ) ?>"><?php the_title() ?></a>
									</td>
								</tr>
							<?php endwhile; ?>
						</table>
					<?php else : ?>
						<p><?php _e( 'Logs not found', 'base' ) ?></p>
					<?php endif;
					wp_reset_postdata();
				endif ?>
			<?php else : ?>
				<?php wp_enqueue_style( 'import-users-styles' );
				wp_enqueue_script( 'import-users-script' ); ?>
				<div id="progress_bar_holder">
					<div id="progress_bar"></div>
				</div>
				
				<form method="post" id="import-users-form" action="<?php echo admin_url( 'users.php?page=import-users' ) ?>" enctype="multipart/form-data">
					<?php wp_nonce_field( 'import-users-from-file-xlsx', 'check-import' ); ?>
					<table class="form-table">
						<tr valign="top">
							<th scope="row"><label for="users_xlsx"><?php _e( 'xlsx file' , 'base'); ?></label></th>
							<td>
								<input type="file" id="users_xlsx" name="users_xlsx" value="">
							</td>
						</tr>
					</table>
					<?php submit_button( __( 'Import' , 'base'), 'primary', 'import-users' ); ?>
					
					<em><a href="<?php echo admin_url( 'users.php?page=import-users&logs=all' ) ?>"><?php _e( 'Logs of import', 'base' ) ?></a></em>
				</form>
			<?php endif ?>
		</div>
		<?php
	}
	
	public static function remove_folder_with_files( $dir ) {
		$files = array_diff( scandir( $dir ), array( '.', '..' ) );
		foreach ( $files as $file ) {
			( is_dir( "$dir/$file" ) ) ? self::remove_folder_with_files( "$dir/$file" ) : unlink( "$dir/$file" );
		}
		return rmdir( $dir );
	}
	
	public static function admin_head() {
		wp_register_style( 'import-users-styles', get_template_directory_uri() . '/import-users-styles.css' );
		wp_register_script( 'import-users-script', get_template_directory_uri() . '/js/import-users-script.js', array( 'jquery' ), '', true );
	}
	
	public static function show_results( $data, $color, $title ) {
		$count = count( $data );
		echo '<h2 style="color:' . $color . ';">' . $title . '</h2>';
		echo '<table class="widefat striped" style="border: 1px solid ' . $color . ';">';
		foreach( $data as $info ){
			echo '<tr>';
			echo '<td>';
			echo isset( $info[ 0 ] ) ? $info[ 0 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 1 ] ) ? $info[ 1 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 2 ] ) ? $info[ 2 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 3 ] ) ? $info[ 3 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 4 ] ) ? $info[ 4 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 5 ] ) ? $info[ 5 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 6 ] ) ? $info[ 6 ] : '&nbsp;';
			echo '</td>';
			echo '<td>';
			echo isset( $info[ 'result' ] ) ? $info[ 'result' ] : '&nbsp;';
			echo '</td>';
			echo '</tr>';
		}
		echo '</table>';
	}
	
}

if( is_admin() ) {
	Import_Users::getInstance()->create_page();
}