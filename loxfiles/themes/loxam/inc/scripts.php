<?php
// Theme css & js

function base_scripts_styles() {
	$in_footer = true;
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	wp_deregister_script( 'comment-reply' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply', get_template_directory_uri() . '/js/comment-reply.js', '', '', $in_footer );
	}

	// Loads JavaScript file with functionality specific.
	wp_enqueue_script( 'base-script', get_template_directory_uri() . '/js/jquery.main.js', array( 'jquery' ), '', $in_footer );
	wp_enqueue_script( 'tween-max', '//cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js', array( 'jquery' ), '', $in_footer );
	wp_enqueue_script( 'impl-script', get_template_directory_uri() . '/js/impl.js', array( 'jquery', 'tween-max' ), '', $in_footer );
	
	wp_enqueue_style( 'base-fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css', array() );
	wp_enqueue_style( 'base-fonts', 'https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900', array() );
	
	// Loads our main stylesheet.
	wp_enqueue_style( 'base-style', get_stylesheet_uri(), array() );
	
	// Implementation stylesheet.
	wp_enqueue_style( 'base-theme', get_template_directory_uri() . '/theme.css', array() );
	
	if( is_page_template( 'pages/template-user-change-password.php' ) or is_page_template( 'pages/template-test.php' ) ) {
		wp_localize_script( 'impl-script', 'themeAjax', 
			array(
				'url' => admin_url( 'admin-ajax.php' ),
				'nonce' => wp_create_nonce( 'theme-ajax-nonce' )
			)
		);  
	}
}
add_action( 'wp_enqueue_scripts', 'base_scripts_styles' );
