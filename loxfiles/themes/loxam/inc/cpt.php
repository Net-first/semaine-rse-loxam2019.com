<?php
// Custom Post Types quiz
add_action('init', 'add_cpt_init');
function add_cpt_init() 
{
    $labels = array(
        'name' => _x('Quizzes', 'post type general name', 'base'),
        'singular_name' => _x('Quiz', 'post type singular name', 'base'),
        'add_new' => _x('Add New', 'quiz', 'base'),
        'add_new_item' => __('Add New Quiz', 'base'),
        'edit_item' => __('Edit Quiz', 'base'),
        'new_item' => __('New Quiz', 'base'),
        'all_items' => __('All Quizzes', 'base'),
        'view_item' => __('View Quiz', 'base'),
        'search_items' => __('Search Quizzes', 'base'),
        'not_found' =>  __('No quizzes found', 'base'),
        'not_found_in_trash' => __('No quizzes found in Trash', 'base'), 
        'parent_item_colon' => '',
        'menu_name' => __( 'Quizzes', 'base' )
    );
    $args = array(
        'labels' => $labels,
        'public' => false,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-editor-help',
        'supports' => array('title','author')
    );

    register_post_type('quiz',$args);
    
    $labels = array(
		'name' => _x('Logs', 'post type general name', 'base'),
		'singular_name' => _x('Log', 'post type singular name', 'base'),
		'add_new' => _x('Add New', 'logs', 'base'),
		'add_new_item' => __('Add New Log', 'base'),
		'edit_item' => __('Edit Log', 'base'),
		'new_item' => __('New Log', 'base'),
		'all_items' => __('All Logs', 'base'),
		'view_item' => __('View Log', 'base'),
		'search_items' => __('Search Logs', 'base'),
		'not_found' =>  __('No logs found', 'base'),
		'not_found_in_trash' => __('No logs found in Trash', 'base'), 
		'parent_item_colon' => '',
		'menu_name' => __( 'Logs', 'base' )
	);
	$args = array(
		'labels' => $labels,
		'public' => false,
		//'show_ui' => true,
		//'capability_type' => 'log',
         //       'map_meta_cap' => true,
		'hierarchical' => false,
		'menu_position' => 100,
		'supports' => array('title','editor','author')
	); 
	register_post_type('import-users-logs',$args);
}

add_filter('post_updated_messages', 'cpt_updated_messages');
function cpt_updated_messages( $messages ) {
    global $post, $post_ID;
  
    $messages['quiz'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf( __('Quiz updated. <a href="%s">View quiz</a>', 'base'), esc_url( get_permalink($post_ID) ) ),
        2 => __('Custom field updated.', 'base'),
        3 => __('Custom field deleted.', 'base'),
        4 => __('Quiz updated.'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf( __('Quiz restored to revision from %s', 'base'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6 => sprintf( __('Quiz published. <a href="%s">View quiz</a>', 'base'), esc_url( get_permalink($post_ID) ) ),
        7 => __('Quiz saved.', 'base'),
        8 => sprintf( __('Quiz submitted. <a target="_blank" href="%s">Preview quiz</a>', 'base'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        9 => sprintf( __('Quiz scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview quiz</a>', 'base'),
          // translators: Publish box date format, see http://php.net/date
          date_i18n( __( 'M j, Y @ G:i', 'base' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
        10 => sprintf( __('Quiz draft updated. <a target="_blank" href="%s">Preview quiz</a>', 'base'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    );
  
    return $messages;
}

