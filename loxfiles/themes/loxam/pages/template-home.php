<?php
/*
Template Name: Home Template
*/
get_header(); ?>
<div class="login-holder">
    <div class="container">
        <h1><?php the_title() ?></h1>
        <?php echo do_shortcode( '[login-with-ajax template="divs-only"]' ) ?>
    </div>
</div>
<div class="main-img">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg-image-6.png" alt="image description">
</div>
<?php get_footer(); ?>