<?php
/*
Template Name: Quiz Template
*/
get_header();
global $footer_rating;

$u_date = theme_get_user_time();
$_user_date = $u_date[ 'user_date' ]; // 20190513
$_user_time = $u_date[ 'user_time' ]; // 130757 10757
		
$test_page_id = get_field( 'option_test_page', 'option' );

$_title = get_the_title();
$_permalink = get_the_permalink( $test_page_id );
$_background = get_field( 'quiz_template_background' );

$query = new WP_Query( array(
	'post_type' => 'quiz',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'ignore_sticky_posts' => true,
        'order' => 'ASC',
        'orderby' => 'meta_value_num',
        'meta_query' => array(
                            array(
                                    'key'     => 'quiz_date',
                                    'value'   => array( DATE_START, DATE_END ),
                                    'compare' => 'BETWEEN',
                            ),
                        ),
		) );
if( $query->have_posts() ) :
    ob_start();
    while ( $query->have_posts() ) : $query->the_post();
        $date = absint( get_post_meta( get_the_ID(), 'quiz_date', true ) );
        $_active = '';
        $_complete = '';
        $color = get_field( 'quiz_color' );
        $icon = get_field( 'quiz_icon' );
        $box_title = get_field( 'quiz_box_title' );

		if( $date ) {
			if( $_user_date == $date and $_user_time >= TIME_OPEN and $_user_time <= TIME_END ){
				$_active = 'active';
				$footer_rating = 1;
			} else if( $_user_date > $date or ( $_user_date == $date and $_user_time > TIME_END ) ) {
				$_complete = 'completed';
				$footer_rating = 1;
			}
		}

        if( $_active ) {
            $_title = get_the_title();
            $_background = get_field( 'quiz_background' );
        }
		
		if( theme_quiz_has_result( get_current_user_id() , $date ) ){
			$_active = '';
			$_complete = 'completed';
		} ?>
        <div class="item <?php echo $color ?> <?php echo $_active ?> <?php echo $_complete ?>">
            <?php if( $_active ) : ?>
                <div class="current-step" style="background-image: url( <?php echo $icon['url'] ?> );">
                    <strong class="title"><?php echo $box_title ?></strong>
                    <span class="sub-title"><?php echo ucfirst( strtolower( date_i18n( 'l j F', strtotime( $date ) ) ) ); // gmt ??? ?></span>
                    <?php if( $_user_time >= TIME_START and $_user_time <= TIME_END ) : ?>
						<span class="more"><?php _e( 'JOUER', 'base' ) ?> <i class="fas fa-chevron-right"></i></span>
						<a href="<?php echo add_query_arg( 'quiz_id', get_the_ID(), $_permalink ) ?>" class="link"></a>
					<?php endif ?>
                </div>
            <?php elseif( $_complete ) : ?>
                <div class="completed-step">
                    <strong class="title"><?php echo $box_title ?></strong>
                    <span class="more"><img src="<?php echo get_template_directory_uri() ?>/images/ico-check.png" alt="image description"> <span><?php _e( 'REVOIR', 'base' ) ?> <i class="fas fa-chevron-right"></i></span></span>
                    <a href="<?php echo add_query_arg( 'quiz_id', get_the_ID(), $_permalink ) ?>" class="link"></a>
                </div>
            <?php else : ?>
                <div class="lock-holder">
                    <div class="ico">
                        <img src="<?php echo get_template_directory_uri() ?>/images/ico-lock.png" alt="image description">
                    </div>
                    <strong><span><?php _e( 'Disponible', 'base' ) ?></span> <?php echo strtolower( date_i18n( 'l j F', strtotime( $date ) ) ); // gmt ??? ?></strong>
                </div>
            <?php endif ?>
        </div>
    <?php endwhile;
    $buffer = ob_get_contents();
    ob_end_clean();
endif;
wp_reset_postdata();
?>
<div class="container">
    <div class="steps-holder">
        <h1><?php echo $_title ?></h1>
        <div class="steps-list">
            <?php echo $buffer ?>
        </div>
    </div>
</div>
<div class="main-img">
    <img src="<?php echo $_background['url'] ?>" alt="<?php echo $_background['alt'] ?>">
</div>
<?php get_footer(); ?>