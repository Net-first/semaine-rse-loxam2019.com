<?php
/*
Template Name: Test Template
*/
get_header();
the_post();

$user_id = get_current_user_id();

$quiz_page_id = get_field( 'option_quiz_page', 'option' );
$_background = get_field( 'quiz_template_background', $quiz_page_id );

$_active = '';
$_complete = '';
$_quiz_id = isset( $_GET['quiz_id'] ) ? absint( $_GET['quiz_id'] ) : 0;
if( $_quiz_id ) {
    $date = absint( get_post_meta( $_quiz_id, 'quiz_date', true ) );

    $u_date = theme_get_user_time();
    $_user_date = $u_date[ 'user_date' ]; // 20190513
    $_user_time = $u_date[ 'user_time' ]; // 130757 10757
    
    if( $date ) {
        if( $_user_date == $date and $_user_time >= TIME_START and $_user_time <= TIME_END ){
            $_active = 1;
        } else if( $_user_date > $date or ( $_user_date == $date and $_user_time > TIME_END ) ) {
            $_complete = 1;
        }
    }
    
    if( $_active or $_complete ){
        $_background = get_field( 'quiz_background', $_quiz_id );
    }
} ?>
<form id="ajax-test-holder">
    <div class="intro-holder">
        <div class="container">
            <?php if( $_active ) : ?>
                <h1><?php the_title() ?></h1>
                <?php the_content(); ?>
                <?php wp_link_pages(); ?>
                <?php if( has_post_thumbnail() ) : ?>
                    <div class="intro-ico">
                        <?php the_post_thumbnail( 'full' ); ?>
                    </div>
                <?php endif ?>
                <?php $button_name = theme_quiz_has_result( $user_id , $date ) ? __( 'Voir les résultats', 'base' ) : __( 'COMMENCER LE TEST', 'base' ) ?>
                
                <button type="submit" class="btn btn-red"><?php echo $button_name ?> <i class="fas fa-chevron-right"></i></button>
                <input type="hidden" name="quiz_id" value="<?php echo $_quiz_id ?>">
                <input type="hidden" name="quiz_action" value="start">
            <?php elseif( $_complete ) :
                theme_quiz_completed_results_html( $_quiz_id, $user_id, $date );
            else : ?>
                <span class="lwa-status lwa-status-invalid" style="text-align: center;"><?php _e( 'Access denied!', 'base' ) ?></span>
            <?php endif ?>
        </div>
    </div>
</form>
<div class="main-img">
    <img src="<?php echo $_background['url'] ?>" alt="<?php echo $_background['alt'] ?>">
</div>
<?php get_footer(); ?>