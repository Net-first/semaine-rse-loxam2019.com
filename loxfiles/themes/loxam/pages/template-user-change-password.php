<?php
/*
Template Name: User Change Password Template
*/
$current_user = wp_get_current_user();

if( isset( $_GET[ 'leave_password' ] ) ) {
    update_user_meta( $current_user->ID, '_activate_account', 1, 0 );
}

$activate_account = get_user_meta( $current_user->ID, '_activate_account', true );
$quiz_page_id = get_field( 'option_quiz_page', 'option' );
if( $quiz_page_id and $activate_account ) {
    wp_redirect( get_permalink( $quiz_page_id ) );
    exit;   
}

$user_agency = get_field( 'user_agency', 'user_' . $current_user->ID );

get_header(); ?>
<div class="login-holder">
    <div class="container">
        <h1><?php _e( 'Bienvenue, Agence', 'base' ) ?> <?php echo $user_agency ? $user_agency : $current_user->data->display_name ?> ! </h1>
        <strong class="sub-title"><?php _e( 'Changez votre mot de passe', 'base' ) ?></strong>
        <form action="#" id="change-pass" method="post">
            <div class="row">
                <label for="current"><?php _e( 'Actuel', 'base' ) ?></label>
                <input type="password" id="current" name="c_pass" placeholder="<?php echo esc_attr( __( 'Entrez le mot de passe actuel', 'base' ) ) ?>">
            </div>
            <div class="row">
                <label for="new-password"><?php _e( 'Nouveau', 'base' ) ?></label>
                <input type="password" id="new-password" name="n_pass" placeholder="<?php echo esc_attr( __( 'Entrez votre nouveau mot de passe', 'base' ) ) ?>">
                <input type="password" id="repeat-password" name="r_pass" placeholder="<?php echo esc_attr( __( 'Confirmez votre mot de passe', 'base' ) ) ?>">
                <a href="<?php echo add_query_arg( 'leave_password', 1, get_permalink() ) ?>" class="link"><?php _e( 'Je ne souhaite pas changer le mot de passe >', 'base' ) ?></a>
            </div>
            <button type="submit" class="btn btn-red"><?php _e( 'VALIDER', 'base' ) ?> <i class="fas fa-chevron-right"></i></button>
        </form>
    </div>
</div>
<div class="main-img">
    <img src="<?php echo get_template_directory_uri() ?>/images/bg-image-6.png" alt="image description">
</div>
<?php get_footer(); ?>