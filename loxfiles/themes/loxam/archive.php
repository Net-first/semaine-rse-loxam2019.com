<?php get_header(); ?>
<div class="login-holder">
    <div class="container">
        <?php if ( have_posts() ) : ?>
			<div class="title">
				<?php the_archive_title( '<h1>', '</h1>' ); ?>
			</div>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'blocks/content', get_post_type() ); ?>
			<?php endwhile; ?>
			<?php get_template_part( 'blocks/pager' ); ?>
		<?php else : ?>
			<?php get_template_part( 'blocks/not_found' ); ?>
		<?php endif; ?>
    </div>
</div>
<div class="main-img">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg-image-6.png" alt="image description">
</div>
<?php get_footer(); ?>