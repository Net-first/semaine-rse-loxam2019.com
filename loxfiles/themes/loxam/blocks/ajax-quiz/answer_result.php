<div class="question-holder">
    <div class="top-holder">
        <div class="container">
            <strong class="question-number"><?php echo $current_question ?>/<?php echo $count_questions ?></strong>
            <h1><?php echo $title ?></h1>
        </div>
    </div>
    <div class="answer-holder">
        <div class="container">
            <div class="frame">
                <h2><?php echo $result_answer_data['title'] ?></h2>
                <?php echo $result_answer_data['text'] ?>
                <div class="btn-holder">
                    <button type="submit" class="btn btn-red"><?php _e( 'Question suivante', 'base' ) ?> <i class="fas fa-chevron-right"></i></button>
                    <input type="hidden" name="quiz_id" value="<?php echo $_quiz_id ?>">
                    <input type="hidden" name="quiz_action" value="start">
                </div>
            </div>
        </div>
    </div>
</div>