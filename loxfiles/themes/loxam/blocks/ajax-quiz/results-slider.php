<?php if( have_rows( 'quiz_questions', $_quiz_id ) ):
    $title = get_field( 'quiz_box_title', $_quiz_id );
    $quiz_questions = count( get_field( 'quiz_questions', $_quiz_id ) );
    $user_result = Quiz_Data::get_questions_data( $user_id, $date );
    $result_data = array();
    if( $user_result ) {
        foreach( $user_result as $u_result ){
            $result_data[ $u_result->question_id ] = $u_result->score;
        }
    }

    $return_link = home_url() ?>
    <div class="slick-slider-frame">
        <?php while( have_rows( 'quiz_questions', $_quiz_id ) ): the_row();
            $_i = acf_get_loop( 'active', 'i' );
            $question = get_sub_field( 'question' );
            $key = ( isset( $result_data[ $_i + 1 ] ) and $result_data[ $_i + 1 ] ) ? 'if_the_correct_answer' : 'if_the_answer_is_incorrect';
            $answer = get_sub_field( $key ); ?>
            <div class="question-holder">
                <div class="top-holder">
                    <div class="container">
                        <strong class="question-number"><?php echo $_i + 1 ?>/<?php echo $quiz_questions ?></strong>
                        <h1><?php echo $title ?></h1>
                    </div>
                </div>
                <div class="answer-holder">
                    <div class="container">
                        <div class="frame">
                            <div class="complete_question">
                                <?php echo $question ?>
                            </div>
                            <?php if( $result_data ) : ?>
                                <h2><?php echo $answer['title'] ?></h2>
                                <?php echo $answer['text'] ?>
                            <?php endif ?>
                            <div class="btn-holder">
                                <a href="<?php echo $return_link ?>" class="btn btn-red"><?php _e( 'Retour', 'loxam' ) ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif;