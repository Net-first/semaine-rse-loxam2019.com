<div class="question-holder">
    <div class="top-holder">
        <div class="container">
            <strong class="question-number"><?php echo $current_question ?>/<?php echo $count_questions ?></strong>
            <h1><?php echo $title ?></h1>
        </div>
    </div>
    <div class="text-holder">
        <div class="container">
            <?php echo $question ?>
        </div>
    </div>
    <div class="form-holder">
        <div class="container">
            <span class="lwa-status"><?php _e( 'Please make a choice', 'base' ) ?></span>
            <div class="line-holder">
                <?php $letter = 'A';
                foreach( $answers as $i => $answer ) : ?>
                    <div class="row">
                        <input type="checkbox" name="u_answer[]" value="<?php echo $letter ?>" id="item-<?php echo $i ?>">
                        <label for="item-<?php echo $i ?>">
                            <strong><?php echo $letter ?></strong>
                            <p><?php echo $answer['answer'] ?></p>
                        </label>
                    </div>
                    <?php $letter++;
                endforeach ?>
            </div>
            <button type="submit" style="display: none;" class="btn btn-red"><?php _e( 'Valider', 'base' ) ?> <i class="fas fa-chevron-right"></i></button>
            <input type="hidden" name="quiz_id" value="<?php echo $_quiz_id ?>">
            <input type="hidden" name="quiz_action" value="check">
            <input type="hidden" name="quiz_current_question_id" value="<?php echo $row_id ?>">
            <input type="hidden" name="quiz_hash" value="<?php echo $hash ?>">
        </div>
    </div>
</div>