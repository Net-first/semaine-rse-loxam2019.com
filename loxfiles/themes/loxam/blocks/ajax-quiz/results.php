<?php $query = new WP_Query( array(
	'post_type' => 'quiz',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'ignore_sticky_posts' => true,
        'order' => 'ASC',
        'orderby' => 'meta_value_num',
        'meta_query' => array(
                            array(
                                    'key'     => 'quiz_date',
                                    'value'   => array( DATE_START, DATE_END ),
                                    'compare' => 'BETWEEN',
                            ),
                        ),
		) );
$g_score_max = 0;
$g_score = 0;
$g_time = 0;
$day_count = 0;

$u_date = theme_get_user_time();
$_user_date = $u_date[ 'user_date' ]; // 20190513
$_user_time = $u_date[ 'user_time' ]; // 130757 10757

if( $query->have_posts() ) :
    ob_start();
    while ( $query->have_posts() ) : $query->the_post();
        $date = absint( get_post_meta( get_the_ID(), 'quiz_date', true ) );
        $d_score = 0;
        $d_score_max = 0;
        $d_time = 0;
        
        $q_data = Quiz_Data::get_questions_data( $user_id, $date );
        if( $q_data ) {
            foreach( $q_data as $data ){
                $d_score += $data->score;
                $g_score += $data->score;
                $d_time += $data->time_diff;
                $g_time += $data->time_diff;
            }
        }

        $quiz_questions = get_field( 'quiz_questions', get_the_ID() );
        $quiz_score = get_field( 'quiz_questions_score', get_the_ID() );
        $g_score_max += $quiz_score * count( $quiz_questions );
        $d_score_max += $quiz_score * count( $quiz_questions );
        
        $_active = '';
        $_complete = '';
        $color = get_field( 'quiz_color' );
        $icon = get_field( 'quiz_icon' );
        $box_title = get_field( 'quiz_box_title' );

        if( $date ) {
            if( $_user_date == $date and $_user_time >= TIME_OPEN and $_user_time <= TIME_END ){
                $_active = 'active';
                $day_count++;
            } else if( $_user_date > $date or ( $_user_date == $date and $_user_time > TIME_END ) ) {
                $_complete = 'active';
                $day_count++;
            }
        } ?>
        <div class="col">
            <div class="holder <?php echo $color ?> <?php echo $_active ?> <?php echo $_complete ?>">
                <?php if( $_active or $_complete ) : ?>
                    <strong><?php echo $box_title ?></strong>
                    <span><?php echo $d_score ?>/<?php echo $d_score_max ?> - <?php echo theme_seconds_to_time( $d_time ) ?></span>
                <?php else : ?>
                    <div class="ico">
                        <img src="<?php echo get_template_directory_uri() ?>/images/ico-lock.png" alt="image description">
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php endwhile;
    $buffer = ob_get_contents();
    ob_end_clean();
endif;
wp_reset_postdata(); ?>

<div class="container">
    <div class="results-holder">
        <h1><?php _e( 'Votre score global :', 'base' ) ?></h1>
        <div class="total-info">
            <strong><?php echo zeroise( $g_score, 2 ) ?>/<?php echo $g_score_max ?> - <?php echo theme_seconds_to_time( $g_time ) ?></strong>
        </div>
        <div class="results-list">
            <?php echo $buffer ?>
        </div>
        <?php if( $day_count >= $query->found_posts ) : ?>
            <div class="text-holder">
                <strong class="sub-title"><?php _e( 'BRAVO !', 'base' ) ?> </strong>
                <?php $message = get_field( 'option_final_message', 'option' );
                $report_title = get_field( 'option_final_message_report_title', 'option' );
                $report_link = get_field( 'option_final_message_report_link', 'option' );
                
                echo $message;
                
                if( $report_title ) : ?>
                    <h2><?php echo $report_title ?></h2>
                <?php endif ?>
                <?php if( $report_link ) : ?>
                    <a href="<?php echo $report_link['url'] ?>" target="<?php echo $report_link['target'] ?>" class="btn btn-red"><?php echo $report_link['title'] ?> <i class="fas fa-chevron-right"></i></a>
                <?php endif ?>
            </div>
        <?php else : ?>
            <h1><?php _e( 'Merci et à demain !', 'base' ) ?></h1>
        <?php endif ?>
    </div>
</div>