<?php 
/*
 * This is the page users will see logged out. 
 * You can edit this, but for upgrade safety you should copy and modify this file into your template folder.
 * The location from within your template folder is plugins/login-with-ajax/ (create these directories if they don't exist)
*/
?>
<div class="lwa lwa-divs-only">
	<span class="lwa-status"></span>
	<form class="lwa-form" action="<?php echo esc_attr(LoginWithAjax::$url_login); ?>" method="post">
		<div class="row">
			<label for="lwa_user_login"><?php esc_html_e( 'Login','base' ) ?></label>
			<input type="text" name="log" placeholder="<?php echo esc_attr( __( 'Entrez votre mail', 'base' ) ) ?>" id="lwa_user_login" />
		</div>

		<div class="row">
			<label for="lwa_user_pass"><?php esc_html_e( 'Mot de passe','base' ) ?></label>
			<input type="password" name="pwd" placeholder="<?php echo esc_attr( __( 'Entrez le mot de passe communiqué par mail', 'base' ) ) ?>" id="lwa_user_pass" />
		</div>
		
		<div class="lwa-login_form">
			<?php do_action('login_form'); ?>
		</div>
   
		<div class="lwa-submit-button">
			<button type="submit" class="btn btn-red"><?php esc_attr_e('VALIDER','base'); ?> <i class="fas fa-chevron-right"></i></button>
			<input type="hidden" name="lwa_profile_link" value="<?php echo esc_attr($lwa_data['profile_link']); ?>" />
			<input type="hidden" name="login-with-ajax" value="login" />
			<?php if( !empty($lwa_data['redirect']) ): ?>
			<input type="hidden" name="redirect_to" value="<?php echo esc_url($lwa_data['redirect']); ?>" />
			<?php endif; ?>
		</div>
	</form>
</div>