��    V      �     |      x  9   y  D   �  B   �  B   ;  9   ~  #   �     �  s   �     c	     �	     �	  '   �	  m   �	     .
     ?
     [
     c
  
   j
  J   u
     �
     �
  5   �
     !     2     C     Y     p  �   s  �   Y  �   �  �   �  �     h   �  i   )     �     �  	   �  
   �      �     �     �  $     C   +     o     �     �  &   �     �     �     �     �     �  )        @     H     ]     f  0   }     �     �  !   �  "   �          ,     C     T     m  v   �     �     �  �     �   �  
   �  2   �  N        `     i     v  [   �     �       $   '  
   L     W  4   f  &  �  Q   �  @     @   U  ;   �  >   �  X        j  {   |     �            *   <  t   g     �  &   �            
   &  X   1  !   �     �  >   �     �          *     D     _  �   e  �   [  �   �  �   �  T   >  �   �  �         �   	   �      �   	   �   .   �      �   1   !  &   @!  C   g!     �!     �!     �!  -   �!  	   "     "     &"  
   E"     P"  (   p"     �"     �"     �"  "   �"  Y   �"      D#  	   e#  !   o#  "   �#     �#     �#     �#     �#     $  �   ,$  	   �$     �$  �   �$    �%  
   �&  :   �&  T   '     d'     s'  !   �'  ]   �'     (     %(  $   ;(     `(     l(  4   {(     J   A   9       
   B       8   :          P            .   H   '   D          U         N   Q      O   4   (      7      R           K   )      $       5          2         V               I       "              +               3          %                 F           ;       #   -       6   C           *      >             <           ?          G              ,                     E      !       0              M   T          L           =   1   &       S   	   /   @    %s will be replaced with a link to set the user password. <code>%BLOGNAME%</code> will be replaced with the name of your blog. <code>%BLOGURL%</code> will be replaced with the url of your blog. <code>%PASSWORD%</code> will be replaced with the user's password. <code>%USERNAME%</code> will be replaced with a username. A password will be e-mailed to you. AJAX Registration? Ajax driven login widget. Customisable from within your template folder, and advanced settings from the admin area. An undefined error has ocurred Cancel Changes saved. Check your registration email template. Choose the default theme you'd like to use. This can be overriden in the widget, shortcode and template tags. Default Template Disable refresh upon login? Dismiss E-mail E-mail: %s Enter %LASTURL% to send the user back to the page they were previously on. Enter username or email Forgotten Password Further documentation for this feature coming soon... General Settings Get New Password Global Login Redirect Global Logout Redirect Hi If the user logs in and you check the button above, only the login widget will update itself without refreshing the page. Not a good idea if your site shows different content to users once logged in, as a refresh would be needed. If this feature doesn't work, please make sure that you don't have another plugin installed which also manages user registrations (e.g. BuddyPress and MU). If you would like a specific user role to be redirected to a custom URL upon login, place it here (blank value will default to the global redirect) If you would like a specific user role to be redirected to a custom URL upon logout, place it here (blank value will default to the global redirect) If you'd like to override the default Wordpress email users receive once registered, make sure you check the box below and enter a new email subject and message. If you'd like to send the user to a specific URL after login, enter it here (e.g. http://wordpress.org/) If you'd like to send the user to a specific URL after logout, enter it here (e.g. http://wordpress.org/) Log In Log Out Logged In Logged Out Login Successful, redirecting... Login With Ajax Login successful, updating... Login widget with AJAX capabilities. Login, Ajax, Redirect, BuddyPress, MU, WPMU, sidebar, admin, widget Lost your password? Marcus Sykes Message New user registration on your site %s: No Link Notification Settings Override Default Email? Password Password Lost and Found Please supply your username and password. Profile Redirection Settings Register Register For This Site Registration complete. Please check your e-mail. Registration has been disabled. Remember Me Role-Based Custom Login Redirects Role-Based Custom Logout Redirects Save Changes Show Recover Password? Show direct link Show link with AJAX form Show profile link? Since WordPress 4.3 passwords are not emailed to users anymore, they're replaced with a link to create a new password. Subject Template Thanks for signing up to our blog. 

You can login with the following credentials by visiting %BLOGURL%

Username : %USERNAME%
Password : %PASSWORD%

We look forward to your next visit!

The team at %BLOGNAME% Thanks for signing up to our blog. 

You can login with the following credentials by visiting %BLOGURL%

Username: %USERNAME%
To set your password, visit the following address: %PASSWORD%

We look forward to your next visit!

The team at %BLOGNAME% Title (%s) To set your password, visit the following address: Use %USERNAME% and it will be replaced with the username of person logging in. Username Username: %s We have sent you an email With WPML enabled you can provide different redirection destinations based on language too. Your registration at %BLOGNAME% [%s] New User Registration [%s] Your username and password info blog admin http://msyk.es http://wordpress.org/extend/plugins/login-with-ajax/ PO-Revision-Date: 2017-04-21 15:32:03+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: nl
Project-Id-Version: Plugins - Login With Ajax - Stable (latest release)
 %s zal worden vervangen door een link naar de gebruiker wachtwoord in te stellen. <code>%BLOGNAME%</code> wordt vervangen met de naam van je blog. <code>%BLOGNAME%</code> wordt vervangen door de url van je blog. <code>%PASSWORD%</code> wordt vervangen met een wachtwoord. <code>%USERNAME%</code> wordt vervangen met een gebruikersnaam We sturen je per e-mail een wachtwoord toe. Soms komt deze e-mail in je spambox terecht. AJAX registratie? Ajax aangedreven login widget. Aanpasbaar van binnen je template map, en geavanceerde instellingen vanuit het admin gebied. Oeps, er is iets misgegaan. Afbreken Veranderingen zijn opgeslagen. Controleer uw registratie e-mail template. Kies je standaard thema die je wil gebruiken. Deze kan overschreden worden in de widget, shortcode en template tags. Standaard template Blokkeer pagina refresh tijdens login? Afwijzen E-mail E-mail: %s Voer %LASTURL% in om de gebruiker terug te sturen naar de pagina waar deze vandaan kwam. gebruikersnaam of e-mail invullen Wachtwoord vergeten Verdere documentatie van deze feature verschijnt binnenkort... Algemene instellingen Wachtwoord aanvragen Algemene inlog verwijzing Algemene uitlog verwijzing Hallo Wanneer een gebruiker inlogt en het bovenstaande hokje is aangevinkt dan wordt alleen de login widget geüpdatet. Dit is geen goed idee wanneer je website meer content aanbied aan gebruikers die zijn ingelogd. Een pagina verversing is dan nodig. Wanneer deze feature niet werkt, controleer dan of je geen andere plugins hebt geïnstalleerd die ook gebruikers registraties beheren (zoals BuddyPress en MU). Als je wilt dat een bepaalde gebruiker rol na het inloggen naar een bepaalde URL wordt doorgestuurd, vul die dan hier in (Vul niks in om de algemene verwijzing) Als je wilt dat een bepaalde gebruiker rol na het uitloggen naar een bepaalde URL wordt doorgestuurd, vul die dan hier in (Vul niks in om de algemene verwijzing) Met WPML geactiveerd kan je verschillende re-directions gebruiken gebaseerd op taal. Als je wilt dat een gebruiker na het inloggen naar een bepaalde URL wordt doorgestuurd, vul die dan hier in (bijv.: http://wordpress.org/) Als je wilt dat een gebruiker na het uitloggen naar een bepaalde URL wordt doorgestuurd, vul die dan hier in (bijv.: http://wordpress.org/) Inloggen Uitloggen Ingelogd Uitgelogd Succesvol ingelogd, u wordt nu doorgestuurd... Login Met Ajax Succesvol ingelogd, de pagina wordt bijgewerkt... Login widget met Ajax functionaliteit. Login, Ajax, Redirect, BuddyPress, MU, WPMU, sidebar, admin, widget Wachtwoord vergeten? Marcus Sykes Bericht Nieuwe gebruiker geregistreerd op de site %s: Geen link Notificatie instellingen Standaard e-mail overschijven? Wachtwoord Wachtwoord vergeten en gevonden Voor je gebruikersnaam en wachtwoord in. Profiel Re-direction instellingen Registreren Maak een account aan op deze site. Registratie voltooid. Controleer je e-mail. Geen e-mail ontvangen? Controleer je spambox. Registraties zijn uitgeschakeld. Onthouden Rol gebaseerde login verwijzingen Rol gebaseerde logout verwijzingen Wijzigingen opslaan Toon Herstel wachtwoord? Toon directe link Toon de link met AJAX formulier Linkje naar het profiel? Sinds WordPress 4.3 wachtwoorden worden niet meer gemaild aan gebruikers, worden ze vervangen door een link naar een nieuw wachtwoord aan te maken. Onderwerp Template Bedankt voor het inschrijven op onze blog. 

Je kan inloggen met de volgende gegevens door %BLOGURL% te bezoeken

Gebruikersnaam : %USERNAME%
Wachtwoord : %PASSWORD%

We kijken uit naar je volgende bezoek!

Het %BLOGNAME% team! Bedankt voor het inschrijven op onze blog. 

Je kan inloggen met de volgende gegevens door %BLOGURL% te bezoeken

Gebruikersnaam : %USERNAME%
Bezoek het volgende adres, om je wachtwoord in te stellen: %PASSWORD%

We kijken uit naar je volgende bezoek!

Het %BLOGNAME% team! Titel (%s) Om je wachtwoord in te stellen, bezoek het volgende adres: Gebruik %USERNAME% en de gebruikersnaam van de persoon die inlogt wordt weergegeven. Gebruikersnaam Gebruikersnaam: %s We hebben je een e-mail gestuurd. Als WPML geactiveerd is, kun je op basis van taal naar verschillende bestemmingen verwijzen.  Jou registratie bij %BLOGNAME% [%s] Nieuwe gebruiker [%s] Je gebruikersnaam en wachtwoord Blog beheer http://msyk.es http://wordpress.org/extend/plugins/login-with-ajax/ 